const fs = require("fs");

function createFiles(cb) {
  try {
    fs.mkdir("jasonFolder", (error) => {
      if (error) {
        return console.log(error);
      }
      for (let index = 0; index < 5; index++) {
        fs.writeFile(
          `./jasonFolder/file${index}.json`,
          `this is file${index}.json`,
          (error) => {
            if (error) {
              console.log(error);
              return;
            }
            cb(index);
          }
        );
      }
    });
  } catch (error) {
    return;
  }
}

function deleteFiles(index) {
  fs.unlink(`./jasonFolder/file${index}.json`, (error) => {
    if (error) {
      console.log(error);
      return;
    }
    console.log(`file${index}.json is deleted`);
  });
}

module.exports = { createFiles, deleteFiles };
