const fs = require("fs");

function problem2(read, write, append) {
  read("./lipsum.txt", (error, data) => {
    if (error) {
      return console.log(error);
    } else {
      let uppercaseData = data.toUpperCase();
      write("file1.txt", uppercaseData, (error) => {
        if (error) {
          return console.log(error);
        } else {
          write("filenames.txt", "file1.txt", (error) => {
            if (error) {
              return console.log(error);
            }
          });
          read("file1.txt", (error, data) => {
            if (error) {
              return console.log(error);
            } else {
              let lowerCaseData = data.toLowerCase();
              let sentences = ""
              lowerCaseData.split(".").forEach((sentence)=>{
                sentences+=`\n${sentence}`
              });
               
              write("file2.txt", sentences, (error) => {
                if (error) {
                  return console.log(error);
                } else {
                  append("file2.txt", (error) => {
                    if (error) {
                      return console.log(error);
                    }
                  });
                  read("file2.txt", (error, data) => {
                    if (error) {
                      return console.log(error);
                    } else {
                      let sortedData = data.split("\n").sort();
                      let sortedString=""
                       sortedData.forEach((eachData)=>{
                        sortedString+=eachData;
                       })

                      write("file3.txt", sortedString, (error) => {
                        if (error) {
                          return console.log(error);
                        } else {
                          append("file3.txt", (error) => {
                            if (error) {
                              return console.log(error);
                            } else {
                              read("filenames.txt", (error, data) => {
                                if (error) {
                                  return console.log(error);
                                } else {
                                  let arrayOfFileNames = data.split("\n");
                                  for (let filename of arrayOfFileNames) {
                                    fs.unlink(filename, (error) => {
                                      if (error) {
                                        return console.log(error);
                                      } else {
                                        console.log(`${filename} is deleted`);
                                      }
                                    });
                                  }
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

function read(path, cb) {
  fs.readFile(path, "utf-8", cb);
}

function write(path, data, cb) {
  fs.writeFile(path, data, cb);
}

function append(dataToAppend, cb) {
  fs.appendFile("filenames.txt", `\n${dataToAppend}`, cb);
}

module.exports = { problem2, read, write, append };
